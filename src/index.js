import React from 'react';
import { render } from 'react-dom';
import './assets/scss/global.scss';
import './assets/scss/fonts.scss';
import './assets/scss/UI.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';

render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
reportWebVitals();
