import React, { useEffect, useState } from 'react';
import Header from "./components/Header/index";
import Filter from "./components/Filter/index";
import CardList from "./components/Card/CardList";
import Banner from "./components/Banner"
import { cards } from "./mock/cards";

export default function App() {
  const [headerState, setHeaderState] = useState('')
  const handleScrollEvent = () => {
    if (window.scrollY >= 1) setHeaderState('header--sticky')
    else setHeaderState('')
  }
  useEffect(() => {
    window.addEventListener('scroll', handleScrollEvent)
    return () => {
      window.removeEventListener('scroll', handleScrollEvent)
    }
  }, [])
  return (
      <div>
        <Header state={headerState} />
        <Banner/>
        <Filter/>
        <CardList cards={ cards }/>
      </div>
  );
}
