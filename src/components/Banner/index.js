import React from 'react';
import '../../assets/scss/banner.scss';

export default function() {
  return(
    <div className="banner">
        <div className="container banner__container">
            <div className="banner__subtitle text-md">
                Barcelona
            </div>
            <div className="banner__title">
                Het hart van Spanje
            </div>
            <div className="banner__text text-md">
                Het bruisende Barcelona. Met het geluid van castagnetten, de geur van gegrilde gamba’s en de fruitige smaak van sangria ben je er in gedachten al!
            </div>
        </div>
    </div>
  )
}
