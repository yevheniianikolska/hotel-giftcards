import React from 'react'
import Rating from '../UI/Rating'

export default function Card( {card} ) {
    return(
        <div className="card">
            <div className="card__img">
                <img loading="lazy" src={ `/cards/${card.image}` }/>
            </div>
            <div className="card__region text-md">{ card.region }</div>
            <div className="card__title">{ card.title }</div>
            <Rating ratingCount={ card.raiting } />
            <div className="card__descr">{ card.description }</div>
            <div className="card__price text-md">{ card.price }</div>
        </div>
    )
}
