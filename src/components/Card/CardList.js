import React from "react";
import '../../assets/scss/card.scss';

import Card from "./Card"

export default function CardList(props) {
    return (
        <div className="card-wrapper">
            <div className="container card-wrapper__container">
                {
                    props.cards.map(card => {
                        return <Card card = { card } key = { card.title }/>
                    })
                }
            </div>
        </div>
    )
}
