import React from "react";
import '../../assets/scss/filter.scss';

import peopleIcon from "../../assets/images/icons/people.svg";
import dateIcon from "../../assets/images/icons/date.svg";
import FilterBtn from "../UI/FilterBtn"

export default function Filter() {
    return (
        <div className="filter">
            <div className="container filter__container">
                <FilterBtn icon={ peopleIcon } title="vr. 19 okt  -  zo. 21 okt" />
                <FilterBtn icon={ dateIcon } title="2 volwassenen  -  0 kinderen" />
            </div>
        </div>
    )
}
