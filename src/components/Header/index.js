import React from "react";
import '../../assets/scss/header.scss';
import logo from '../../assets/images/logo.svg';
import Button from '../UI/Button'

export default function Header({ state }) {
    const handleOpenMenu = (e) => {
        e.target.classList.toggle('burger--open')
    }
    return (
        <div className={`header ${state}`}>
            <div className="container header__container">
                <a className="logo" href="#">
                    <img loading="lazy" src={ logo }  alt="logo" />
                </a>
                <div className="burger" onClick={(e) => handleOpenMenu(e)}>
                    <div className="burger__item"></div>
                </div>
                <div className="nav">
                    <a href="#" className="link nav__link">Zakelijk</a>
                    <a href="#" className="link nav__link">Saldo check</a>
                    <Button type="primary" text="Geef een hotelgiftcard" />
                    <Button type="accent" text="Boek een hotel" />
                </div>
            </div>
        </div>
    )
}
