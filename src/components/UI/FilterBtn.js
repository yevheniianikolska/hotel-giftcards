import React from 'react';

export default function({ icon, title }) {
  return(
    <div className="filter__item">
        <img loading="lazy" src={ icon } className="filter__item-icon" alt="people" />
        <div className="filter__item-text">{ title }</div>
    </div>
  )
}