import React, { useState, useEffect } from "react";
import star from "../../assets/images/icons/star.svg";

export default function({ ratingCount }) {
  const [rating, setRating] = useState([])

  useEffect(() => {
    for (let i = 0; i < ratingCount; i++) {
      setRating((prev) => [...prev, <img className="card__raiting-item" loading="lazy" src={ star } key={i} alt="star" />])
    }
  }, [])
  return (
    <div className="card__raiting">
      {rating.map(img => img)}
    </div>
  )
}
