import React from 'react';

export default function({ type, text }) {
  return (
    <button className={`btn btn--${type} nav__btn `}>
      <span>{text}</span>
    </button>
  )
}